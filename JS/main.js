const p = (t) => {
    console.log(t);
};
const mainScripts = [
    {
        src: "./JS/fullScreen.js"
    },
    {
        src: "./JS/navigate.js"
    },
    {
        src: "./JS/getLatestVideos.js"
    },
];

const getScript = (scriptProp) => {
    var script = document.createElement("script");
    script.src = scriptProp['src'];
    if (scriptProp['integrity'] !== undefined)
        script.integrity = scriptProp['integrity'];
    if (scriptProp['crossorigin'] !== undefined)
        script.setAttribute('crossorigin', scriptProp['crossorigin']);
    document.head.appendChild(script);
};
const getScripts = (scripts) => {
    scripts.forEach(element => {
        getScript(element);
    });
};
var removeScript = (scriptList) => {
    var allScriptTags = document.querySelectorAll('script');
    scriptList.forEach(ele => {
        allScriptTags.forEach(element => {
            if (ele.src === element.src)
                element.parentNode.removeChild(element);
        });
    })
    console.log('removed', scriptList);
}
const gete = (selector, parent = document) => {
    return parent.querySelector(selector);
}
const el=(ele,event,fun)=>{
    ele.addEventListener(event,fun);
};
const setCSSvar = (property, value) => {
    property = '--' + property;
    document.documentElement.style
        .setProperty(property, value);
};
getScripts(mainScripts);
const video = gete('#video');
const background=gete('.background-img');
video.play();

const t = new TimelineMax();
