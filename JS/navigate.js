const scrollTo = (element) => {
    window.scroll({
        behavior: 'smooth',
        left: 0,
        top: element.offsetTop
    });
};
el(gete('.projects'), 'click', () => scrollTo(gete('.recent-project-section')));
el(gete('.about'), 'click', () => scrollTo(gete('.about-section')));
el(gete('.services-tab'), 'click', () => scrollTo(gete('.services')));



