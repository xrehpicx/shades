var currentPage = 'home';
const t = new TimelineMax();
function cView(view) {
    const linktag = document.querySelector('.style-tag');
    currentPage = view;
    if (view === 'home') {
        currentPage = 'home';
        t.to('.cover', .8, { width: '100%', ease: Power4.easeInOut, onComplete: () => linktag.setAttribute("href", './css/main.css') })
            .to('.cover', .8, { width: '0%', ease: Power4.easeInOut });
    } else if (view === 'showreel') {
        history.pushState({}, '', './index.html');
        t.to('.cover', 1, { width: '100%', ease: Power4.easeInOut, onComplete: () => linktag.setAttribute("href", './css/showreelView.css') }, '-=.5')
            .to('.cover', 1, { width: '0%', ease: Power4.easeInOut });
    }
}
function introAnim() {
    if (currentPage === 'home') {
        t.to('.main1', 1, { className: 'main2', ease: Power4.easeInOut }, '+=1')
            .fromTo('.shadesTxt', 1, { fontSize: '5em' }, { fontSize: '2em', ease: Power4.easeInOut }, '-=1')
            .fromTo('#play-btn', .5, { opacity: '0' }, { opacity: '1', ease: Power2.easeInOut }, '-=.4')
            .fromTo('.project-btn', .5, { opacity: '0' }, { opacity: '1', ease: Power2.easeInOut }, '-=.4')
            .fromTo('.about-btn', .5, { opacity: '0' }, { opacity: '1', ease: Power2.easeInOut }, '-=.4')
            .fromTo('.contact-btn', .5, { opacity: '0' }, { opacity: '1', ease: Power2.easeInOut }, '-=.4');
    }

}
/* EVENT LISTENERS */
/* all hovers and leaves*/
let btns = document.querySelectorAll('.section2 ul li');
const listen = (btnss) => {

    btnss.forEach((btn) => {
        btn.addEventListener('mouseover', () => {
            t.to(btn, .2, { transform: 'scale(1.1)' });
            console.log(btn);
        });
        btn.addEventListener('mouseleave', () => {
            t.to(btn, .2, { transform: 'scale(1)' });
        });
    });
};

/* all the clicks */
//MAIN PAGE BTNS
btns[0].addEventListener('click', () => {
    cView('showreel');
    listen(document.querySelectorAll('.btns li'));
    document.querySelector('.showreel-video video').play();
});
btns[1].addEventListener('click', () => { cView('showreel'); listen(document.querySelectorAll('.btns li')); });
btns[2].addEventListener('click', () => { cView('showreel'); listen(document.querySelectorAll('.btns li')); });
btns[3].addEventListener('click', () => { cView('showreel'); listen(document.querySelectorAll('.btns li')); });

//SHOWREEL PAGE BTNS
const showreelbtns = document.querySelectorAll('.btns li');
showreelbtns[0].addEventListener('click', () => { cView('home'); });
showreelbtns[1].addEventListener('click', () => { cView('showreel'); listen(document.querySelectorAll('.btns li')); });
showreelbtns[2].addEventListener('click', () => { cView('showreel'); listen(document.querySelectorAll('.btns li')); });


//photo transition function
function transit() {
    const section = document.querySelector('.section1');
    setInterval(() => {
        section.style.background = 'white';
        setTimeout(() => section.style.background = 'transparent', 500);
    }, 5000);
}

//setup functions
listen(btns);
introAnim();
//transit();